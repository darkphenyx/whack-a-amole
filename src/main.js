import Vue from 'vue'

import * as firebase from 'firebase'
import { firestorePlugin } from 'vuefire'
import 'firebase/firestore'

import App from './App.vue'
import router from './router'
import store from './store'

import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'

Vue.config.productionTip = false

Vue.use(firestorePlugin)
Vue.use(VueSweetalert2)

let app = ''
const config = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  storageBucket: '',
  messagingSenderId: process.env.VUE_APP_MESSAGINF_SENDER_ID,
  appId: process.env.VUE_APP_APP_ID
}

export const db = firebase.initializeApp(config).firestore().collection('scores')

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
