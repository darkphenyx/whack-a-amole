import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: '',
    difficulty: 0,
    timer: 0,
    levels: [
      {
        name: 'Level 1',
        value: 1
      },
      {
        name: 'Level 2',
        value: 2
      },
      {
        name: 'Level 3',
        value: 3
      }
    ],
    times: [
      {
        name: '20 Seconds',
        value: 20
      },
      {
        name: '30 Seconds',
        value: 30
      },
      {
        name: '40 Seconds',
        value: 40
      },
      {
        name: '50 Seconds',
        value: 50
      },
      {
        name: '60 Seconds',
        value: 60
      },
      {
        name: '120 Seconds',
        value: 120
      }
    ],
    score: 0,
    possibleImages: ['1', '2', '3', '4'],
    whacka: 'mole',
    title: 'Whack-A-Mole'
  },
  mutations: {
    CHANGE_IMAGES: (state) => {
      state.possibleImages = ['5', '6', '7']
      state.whacka = 'vole'
      state.title = 'Whack-A-Vole'
    },
    UPDATE_USERNAME: (state, payload) => {
      state.username = payload
    },
    UPDATE_DIFFICULTY: (state, payload) => {
      state.difficulty = payload
    },
    UPDATE_TIMER: (state, payload) => {
      state.timer = payload
    },
    UPDATE_SCORE: (state) => {
      state.score++
    },
    RESET_SCORE: (state) => {
      state.score = 0
    },
    RESET_GAME: (state) => {
      state.possibleImages = ['1', '2', '3', '4']
      state.whacka = 'mole'
      state.title = 'Whack-A-Mole'
    }
  },
  actions: {
    updateUsername: (context, payload) => {
      context.commit('UPDATE_USERNAME', payload)
    },
    updateDifficulty: (context, payload) => {
      context.commit('UPDATE_DIFFICULTY', parseInt(payload))
    },
    updateTimer: (context, payload) => {
      context.commit('UPDATE_TIMER', parseInt(payload))
    },
    updateScore: context => {
      context.commit('UPDATE_SCORE')
    },
    resetScore: context => {
      context.commit('RESET_SCORE')
    },
    changePossiblePics: context => {
      context.commit('CHANGE_IMAGES')
    },
    resetGame: context => {
      context.commit('RESET_GAME')
    }
  },
  getters: {
    getUsername: state => state.username,
    getDifficulty: state => state.difficulty,
    getTimer: state => state.timer,
    getTimes: state => state.times,
    getLevels: state => state.levels,
    getScore: state => state.score,
    getImagePossibilities: state => state.possibleImages,
    getWhack: state => state.whacka,
    getTitle: state => state.title
  }
})
