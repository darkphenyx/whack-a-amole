'use strict'

import {
  app,
  protocol,
  BrowserWindow,
  ipcMain,
  dialog,
  Menu
} from 'electron'
import {
  createProtocol,
  installVueDevtools
} from 'vue-cli-plugin-electron-builder/lib'
import path from 'path'
const isDevelopment = process.env.NODE_ENV !== 'production'
const log = require('electron-log');

// Logging
log.info('App starting...');

// Error handle function to log error and restart the application
process.on('uncaughtException', (err) => {
  const messageBoxOptions = {
    type: 'error',
    title: 'Error in Main process',
    message: 'Something has went wrong, the application will restart.'
  };
  log.error(err);
  dialog.showMessageBox(messageBoxOptions, () => {
    app.relaunch();
    app.quit();
  });
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ fullscreen: true,
    resizable: false,
    icon: path.join(__static, 'icon.png'),
    webPreferences: {
      nodeIntegration: true
    } })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  win.on('closed', () => {
    win = null
  })

  const menu = Menu.buildFromTemplate([
    {
      label: 'Menu',
      submenu: [
        {
          label: 'Exit',
          click() {
            app.quit()
          },
          accelerator: 'CmdOrCtrl+Q'
        }
      ]
    }
  ])
  Menu.setApplicationMenu(menu)
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installVueDevtools()
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

ipcMain.on('check-pass', (event, pass) => {
  const hashed = require('crypto').createHash('md5').update(pass).digest("hex")
  if (hashed === process.env.VUE_APP_ADMIN_SECRET) {
    win.webContents.send('send-response-pass', true)
  } else {
    win.webContents.send('send-response-pass', false)
  }
})
