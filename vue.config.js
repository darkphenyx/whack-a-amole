module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/assets/styles/globals.scss";
        `,
      }
    }
  },
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        linux: {
          category: 'Game'
        },
        mac: {
          category: 'public.app-category.games'
        },
        win: {
          target: [
            {
              target: 'nsis',
            },
          ],
        },
        nsis: {
          oneClick: true,
          perMachine: true,
        },
      }
    }
  }
}
