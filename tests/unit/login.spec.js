import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Login from "../../src/views/Login";
import * as firebase from 'firebase'

const firebasemock = require('firebase-mock')
const mockauth = new firebasemock.MockAuthentication()

const config = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  storageBucket: '',
  messagingSenderId: process.env.VUE_APP_MESSAGINF_SENDER_ID,
  appId: process.env.VUE_APP_APP_ID
}

firebase.initializeApp(config)

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Login', () => {
  let getters
  let store

  beforeEach(() => {
    getters = {
      getTitle: () => 'mole'
    }

    store = new Vuex.Store({
      getters
    })
  })

  const wrapper = shallowMount(Login, {
    stubs: ['router-link'],
    mocks: {
      mockauth
    },
    store,
    localVue
  })

  const buttonRef = wrapper.find('button')

  wrapper.setData({ email: 'test@gmail.com', password: 'password1' })

  it('should exist', () => {
    expect(wrapper.name()).toBe('login')
  })

  it('should be the correct button element', () => {
    expect(buttonRef.classes()).toContain('login__button')
  })

  it('should handle a click event', () => {
    expect(buttonRef.trigger('click'))
  })
})
