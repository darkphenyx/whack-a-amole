import { shallowMount } from '@vue/test-utils'
import Mole from "../../src/components/Mole";

describe('Mole', () => {
  const wrapper = shallowMount(Mole, {
    propsData: {
      pic: '1'
    }
  })

  it('should exist', () => {
    expect(wrapper.exists()).toBe(true)
  });

  const imgArray = wrapper.findAll('img')

  it('should have pic props', () => {
    expect(wrapper.props().pic).toBe('1')
  })

  it('should contain the following class', () => {
    expect(wrapper.classes()).toContain('mole')
    expect(imgArray.at(0).classes()).toContain('mole__picture')
  })

  it('should have the proper src reference', () => {
    expect(imgArray.at(0).attributes('src')).toBe('../assets/mole1.png')
  });
})
