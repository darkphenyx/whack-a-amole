import { shallowMount } from '@vue/test-utils'
import ScoreItem from "../../src/components/ScoreItem";

describe('ScoreItem', () => {
  const wrapper = shallowMount(ScoreItem, {
    propsData: {
      name: 'foo',
      score: 1,
      appearance: 2,
      timer: 20
    }
  })

  it('should exist', () => {
    expect(wrapper.exists()).toBe(true)
  });

  const divArray = wrapper.findAll('div')

  it('should have the following props', () => {
    expect(wrapper.props().name).toBe('foo')
    expect(wrapper.props().score).toBe(1)
    expect(wrapper.props().appearance).toBe(2)
    expect(wrapper.props().timer).toBe(20)
  })

  it('should contain the following class\'s', () => {
    expect(wrapper.classes()).toContain('scoreItem')
    expect(divArray.at(1).classes()).toContain('scoreItem__name')
    expect(divArray.at(2).classes()).toContain('scoreItem__score')
    expect(divArray.at(3).classes()).toContain('scoreItem__appearance')
    expect(divArray.at(4).classes()).toContain('scoreItem__timer')
  });
})
